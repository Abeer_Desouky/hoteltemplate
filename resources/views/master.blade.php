<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Task</title>
       <link href="{{ URL::asset('css/bootstrap.min.css') }}" rel="stylesheet">
       <link href="{{ URL::asset('css/main.css') }}" rel="stylesheet">
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
           
            .full-height {
                height: 100vh;
            }

            
        </style>
    </head>
    <body>
       
<div class="container">
    
   <!--  start Nav   --> 
       <div class="row justify-content-md-center">
       <div class="col-md-10 offset-md-1">
       <nav class="navbar  navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">paraselsus</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>


  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item ">
        <a class="nav-link" href="#">5-BEREICHE  </a>
      </li>
     
      <li class="nav-item">
        <a class="nav-link" href="#content2">  PROGRAMM    </a>
      </li>
      
      <li class="nav-item">
        <a class="nav-link" href="#content4">  REFERENTEN    </a>
      </li>
      
       <li class="nav-item">
        <a class="nav-link" href="#"> HOTELS    </a>
      </li>
      
       <li class="nav-item">
        <a class="nav-link" href="#content6">  ISO-ZERTIFIZIERUNG   </a>
      </li>
      
      <li class="nav-item">
        <a class="nav-link" href="#">   SPONSORN & PARTNER   </a>
      </li>
     
      <li class="nav-item">
        <a class="nav-link" href="#content7">  TAGUNGSORT         </a>
      </li>
      
       <li class="nav-item">
        <a class="nav-link" href="#content8">    KONTAKT       </a>
      </li>
    </ul>

  </div>
</nav>
       
       </div>
       </div>
        <!-- End Nav -------------  -->

 @include('content1')

@include('content2')
@include('content4')
@include('content6')
@include('content7')
@include('content8')

<footer id="footer">
    
         <div class="row footer align-content-center">
        <div class="col-md-8 offset-md-2 col-sm-12 " >
        <p style="color:#FFF; text-align:center; margin-top:2%;">Impressum  |  Datenschutz</p>
        </div>
        </div> 

</footer>

</div>
     
     <script src="{{ URL::asset('js/jquery-3.1.1.min.js') }}" type="text/javascript"></script>
     <script src="{{ URL::asset('js/bootstrap.min.js') }}" type="text/javascript"></script>
     <script src="{{ URL::asset('js/main.js') }}" type="text/javascript"></script>      
    </body>
</html>
